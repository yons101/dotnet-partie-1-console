namespace App {
    interface AccountService 
    {
        void AddNewAccount(Account account);
        List<Account> GetAllAccounts();
        Account? GetAccountById(int id);
        List<Account> GetDebitedAccounts();
        double GetBalanceAVG();
    }
}