﻿namespace App
{
    class Program
    {
        static void Main(string[] args)
        {
            AccountServiceImpl accountServiceImpl = new AccountServiceImpl();

            System.Console.WriteLine("\n------------- AddNewAccount -------------\n");
            accountServiceImpl.AddNewAccount(new Account(1, "ahmed", 5000, false));
            accountServiceImpl.AddNewAccount(new Account(2, "saad", 2000, false));
            accountServiceImpl.AddNewAccount(new Account(3, "fadwa", 11000, false));
            accountServiceImpl.AddNewAccount(new Account(4, "karim", 4000, true));

            System.Console.WriteLine("\n------------- GetAllAccounts -------------\n");
            foreach (Account item in accountServiceImpl.GetAllAccounts())
            {
                System.Console.WriteLine(item);
            }
            System.Console.WriteLine("\n------------- GetAccountById -------------\n");
            System.Console.WriteLine(accountServiceImpl.GetAccountById(11));
            System.Console.WriteLine("\n------------- GetDebitedAccounts -------------\n");
            foreach (Account item in accountServiceImpl.GetDebitedAccounts())
            {
                System.Console.WriteLine(item);
            }
            System.Console.WriteLine("\n------------- GetBalanceAVG -------------\n");
                System.Console.WriteLine("\nAVG : " + accountServiceImpl.GetBalanceAVG());

        }
    }
}


