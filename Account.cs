namespace App
{
    class Account
    {
        public int ID { get; set; }
        public string? name { get; set; }
        public double balance { get; set; }
        public bool isDebited { get; set; }

        public Account()
        {
            this.isDebited = false;
        }

        public Account(int ID, string name, double balance, bool isDebited)
        {
            this.ID = ID;
            this.name = name;
            this.balance = balance;
            this.isDebited = isDebited;
        }

        public override string? ToString()
        {
            return ID + " - " + name + " - " + balance + " - " + (isDebited ? "debited" : "not debited");
        }
    }
}