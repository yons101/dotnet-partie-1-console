using App;

class AccountServiceImpl : AccountService
{

    private List<Account> list;

    public AccountServiceImpl()
    {
        list = new List<Account>();
    }
    public void AddNewAccount(Account account)
    {
        this.list.Add(account);
        System.Console.WriteLine("Account with ID : " + account.ID + " has been added");
    }

    public Account? GetAccountById(int id)
    {
        foreach (Account account in list)
        {
            if (account.ID == id)
            {
                return account;
            }

        }
        System.Console.WriteLine("Account with ID : " + id + " doesn't exist");
        return null;
    }

    public List<Account> GetAllAccounts()
    {
        return list;
    }

    public double GetBalanceAVG()
    {
        double balance = 0;
        int count = list.Count;

        foreach (Account account in list)
        {
            balance += account.balance;
        }

        return balance / count;
    }

    public List<Account> GetDebitedAccounts()
    {
        List<Account> debitedAccountsList = new List<Account>();

        foreach (Account account in list)
        {
            if (account.isDebited)
            {
                debitedAccountsList.Add(account);
            }
        }

        return debitedAccountsList;
    }
}